document.addEventListener("DOMContentLoaded", () => {
    if (document.querySelector(".stock__slider__container") != undefined) {
        const swiperStocks = new Swiper('.stock__slider__container', {
            slidesPerView: 2,
            speed: 400,
            spaceBetween: 115,
            breakpoints: {
                1651: {
                    spaceBetween: 115
                },

                1451: {
                    spaceBetween: 30
                },

                0: {
                    spaceBetween: 0,
                    slidesPerView: 1
                }
            },
            navigation: {
                nextEl: ".stock__slider__btn_right",
                prevEl: ".stock__slider__btn_left"
            }
        });
    }

    if (document.querySelector(".brands__slider__container") != undefined) {
        const swiperBrands = new Swiper('.brands__slider__container', {
            slidesPerView: 6,
            speed: 400,
            spaceBetween: 20,
            breakpoints: {
                1841: {
                    slidesPerView: 6
                },

                1511: {
                    slidesPerView: 5
                },

                1001: {
                    slidesPerView: 4
                },

                901: {
                    slidesPerView: 3
                },

                601: {
                    slidesPerView: 2
                },

                0: {
                    slidesPerView: 1
                }
            },
            pagination: {
                el: ".brands__slider__container__pagination",
                type: "bullets"
            }
        });
    }

    if (document.querySelector(".lookbook__container__forMobile__mainContainer") != undefined) {
        const swiperBrands = new Swiper('.lookbook__container__forMobile__mainContainer', {
            slidesPerView: 1,
            speed: 400,
            spaceBetween: 20,
            // breakpoints: {
            //     1841: {
            //         slidesPerView: 6
            //     },

            //     1511: {
            //         slidesPerView: 5
            //     },

            //     1001: {
            //         slidesPerView: 4
            //     },

            //     901: {
            //         slidesPerView: 3
            //     },

            //     0: {
            //         slidesPerView: 2
            //     }
            // },
            navigation: {
                nextEl: ".lookbook__container__forMobile__btn_right",
                prevEl: ".lookbook__container__forMobile__btn_left"
            }
        });
    }

    if (document.querySelector(".catalogProduct__container__content__container__item__img__slider") != undefined) {
        const swiperCatalogItem = new Swiper('.catalogProduct__container__content__container__item__img__slider', {
            slidesPerView: 1,
            speed: 400,
            navigation: {
                nextEl: ".catalogProduct__container__content__container__item__img__slider__btnRight",
                prevEl: ".catalogProduct__container__content__container__item__img__slider__btnLeft"
            }
        });
    }

    if (document.querySelector(".item__container__slider") != undefined) {
        const swiperCatalogItem = new Swiper('.item__container__slider', {
            slidesPerView: 5,
            speed: 400,
            spaceBetween: 30,
            navigation: {
                nextEl: ".item__container__slider__headline__pagination__btn_btnRight",
                prevEl: ".item__container__slider__headline__pagination__btn_btnLeft"
            }
        });
    }

    if (document.querySelector(".content__container__slider") != undefined) {
        const swiperCatalogItem = new Swiper('.content__container__slider', {
            slidesPerView: 4,
            speed: 400,
            spaceBetween: 7,
            pagination: {
                el: ".content__container__slider__pagination",
                type: "bullets"
            }
        });
    }

    if (document.querySelector(".peopleInternal__container__content__slider1") != undefined) {
        const swiperCatalogItem = new Swiper('.peopleInternal__container__content__slider1', {
            slidesPerView: 4,
            speed: 400,
            spaceBetween: 30,
            navigation: {
                nextEl: ".peopleInternal__container__content__slider1__navigation__btn_btnRight",
                prevEl: ".peopleInternal__container__content__slider1__navigation__btn_btnLeft"
            }
        });
    }

    if (document.querySelector(".peopleInternal__container__content__slider2") != undefined) {
        const swiperCatalogItem = new Swiper('.peopleInternal__container__content__slider2', {
            slidesPerView: 5,
            speed: 400,
            spaceBetween: 30,
            navigation: {
                nextEl: ".peopleInternal__container__content__slider2__header__navigation__btn_btnRight",
                prevEl: ".peopleInternal__container__content__slider2__header__navigation__btn_btnLeft"
            }
        });
    }
});