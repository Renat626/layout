document.addEventListener("DOMContentLoaded", () => {
    console.log(window.innerWidth);
    let zoom1 = 16.5;
    let zoom2 = 16.5;
    let zoom3 = 16.5;

    if (window.innerWidth > 1769) {
        zoom1 = 16.5;
        zoom2 = 16.5;
        zoom3 = 16.5;
    } else if (window.innerWidth > 1550 && window.innerWidth < 1769) {
        zoom1 = 17.5;
        zoom2 = 17.5;
        zoom3 = 17.5;
    } else if (window.innerWidth > 1411 && window.innerWidth < 1550) {
        zoom1 = 18.5;
        zoom2 = 17.5;
        zoom3 = 18.5;
    } else if (window.innerWidth > 955 && window.innerWidth < 1411) {
        zoom1 = 18.5;
        zoom2 = 16.5;
        zoom3 = 17.5;
    } else if (window.innerWidth > 600 && window.innerWidth < 956) {
        zoom1 = 17.5;
        zoom2 = 16.5;
        zoom3 = 17.5;
    }

    ymaps.ready(function () {
        let map1 = new ymaps.Map('map1', {
            center: [55.158915, 61.405362],
            zoom: zoom1
        }, {
            searchControlProvider: 'yandex#search'
        }),

            myPlacemarkForMap1 = new ymaps.Placemark([55.158740, 61.407036], {
                hintContent: 'Собственный значок метки',
                balloonContent: 'Это красивая метка'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'assets/img/iconForMap.png',
                // Размеры метки.
                iconImageSize: [84, 95],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-50, -98]
            });

        map1.geoObjects.add(myPlacemarkForMap1);
        map1.behaviors.disable('scrollZoom')

        let map2 = new ymaps.Map('map2', {
            center: [55.189573, 61.290452],
            zoom: zoom2
        }, {
            searchControlProvider: 'yandex#search'
        }),

            myPlacemarkForMap2 = new ymaps.Placemark([55.189270, 61.295483], {
                hintContent: 'Собственный значок метки',
                balloonContent: 'Это красивая метка'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'assets/img/iconForMap.png',
                // Размеры метки.
                iconImageSize: [84, 95],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-50, -98]
            });

        map2.geoObjects.add(myPlacemarkForMap2);
        map2.behaviors.disable('scrollZoom')

        let map3 = new ymaps.Map('map3', {
            center: [55.170842, 61.372062],
            zoom: zoom3
        }, {
            searchControlProvider: 'yandex#search'
        }),

            myPlacemarkForMap3 = new ymaps.Placemark([55.170784, 61.374562], {
                hintContent: 'Собственный значок метки',
                balloonContent: 'Это красивая метка'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: 'assets/img/iconForMap.png',
                // Размеры метки.
                iconImageSize: [84, 95],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-50, -98]
            });

        map3.geoObjects.add(myPlacemarkForMap3);
        map3.behaviors.disable('scrollZoom')
    });
});