document.addEventListener("DOMContentLoaded", () => {
    
    // show address of another city.

    let citiesHeaders = document.querySelectorAll(".header__container__menu__blockOfShop__container1__city");
    let citiesAddressHeaders = document.querySelectorAll(".header__container__menu__blockOfShop__container2__address");

    for (let i = 0; i < citiesHeaders.length; i++) {
        citiesHeaders[i].addEventListener("click", () => {
            if (!citiesHeaders[i].classList.contains("header__container__menu__blockOfShop__container1__city_js")) {
                citiesHeaders[i].classList.add("header__container__menu__blockOfShop__container1__city_js")

                for (let j = 0; j < citiesAddressHeaders.length; j++) {
                    if (citiesHeaders[i].dataset.city == citiesAddressHeaders[j].dataset.city) {
                        citiesAddressHeaders[j].classList.add("header__container__menu__blockOfShop__container2__address_js");
                    }

                    if (citiesHeaders[i].dataset.city != citiesAddressHeaders[j].dataset.city && citiesAddressHeaders[j].classList.contains("header__container__menu__blockOfShop__container2__address_js")) {
                        citiesAddressHeaders[j].classList.remove("header__container__menu__blockOfShop__container2__address_js");
                    }
                }

                for (let j = 0; j < citiesHeaders.length; j++) {
                    if (citiesHeaders[i].dataset.city != citiesHeaders[j].dataset.city && citiesHeaders[j].classList.contains("header__container__menu__blockOfShop__container1__city_js")) {
                        citiesHeaders[j].classList.remove("header__container__menu__blockOfShop__container1__city_js")
                    }
                }
            }
        })
    }

    // show mobile menu.

    let burgerBtn = document.querySelector(".header__container__btn>i");
    let mobile = document.querySelector(".header__container__part2");
    let headerContainer = document.querySelector(".header__container");

    burgerBtn.addEventListener("click", () => {
        if (!mobile.classList.contains("header__container__part2_js")) {
            mobile.classList.add("header__container__part2_js");
            headerContainer.classList.add("header__container_paddingBottomJs");
        } else {
            mobile.classList.remove("header__container__part2_js");
            headerContainer.classList.remove("header__container_paddingBottomJs");
        }
    })

    // show search

    const btnForShowSearch = document.querySelector(".header__container__search>button");
    let search = document.querySelector(".header__container__searchBlock");

    btnForShowSearch.addEventListener("click", () => {
        search.classList.add("header__container__searchBlock_blockJs");
    })

    // button for scroll up

    let buttonForScrollUp = document.querySelector(".scrollUp");

    window.addEventListener("scroll", () => {
        if (window.scrollY > 175) {
            buttonForScrollUp.classList.add("scrollUp_js");
        } else {
            buttonForScrollUp.classList.remove("scrollUp_js");
        }
    })

    // scroll header

    let scrollHeader = document.querySelector(".header_scroll");

    window.addEventListener("scroll", () => {
        if (window.scrollY > 175) {
            scrollHeader.classList.add("header_scroll_js");
        } else {
            scrollHeader.classList.remove("header_scroll_js");
        }
    })

    // show mobile menu scroll.

    let burgerBtnScroll = document.querySelector(".header_scroll__container__btn>i");
    let mobileScroll = document.querySelector(".header_scroll__container__block2");

    burgerBtnScroll.addEventListener("click", () => {
        if (!mobileScroll.classList.contains("header_scroll__container__block2_js")) {
            mobileScroll.classList.add("header_scroll__container__block2_js");
            scrollHeader.classList.add("header_scroll_paddingBottomJs");
        } else {
            mobileScroll.classList.remove("header_scroll__container__block2_js");
            scrollHeader.classList.remove("header_scroll_paddingBottomJs");
        }
    })

    // change map

    let address = document.querySelectorAll(".map__contacts__block__address");
    let maps = document.querySelectorAll(".map__content__block");

    if (address.length > 0) {
        for (let i = 0; i < address.length; i++) {
            address[i].addEventListener("click", () => {
                for (let j = 0; j < maps.length; j++) {
                    if (address[i].dataset.map == maps[j].id) {
                        address[i].classList.add("map__contacts__block__address_active");
                        maps[j].classList.add("map__content__block_active");
                    }
    
                    if (maps[j].classList.contains("map__content__block_active") && address[i].dataset.map != maps[j].id) {
                        maps[j].classList.remove("map__content__block_active");
                    }
                }
    
                for (let j = 0; j < address.length; j++) {
                    if (address[i] != address[j] && address[j].classList.contains("map__contacts__block__address_active")) {
                        address[j].classList.remove("map__contacts__block__address_active")
                    }
                }
            })
        }
    }

    let cities = document.querySelectorAll(".map__contacts__changeCity>p");
    let citiesBlock = document.querySelectorAll(".map__contacts__block");

    if (cities.length > 0) {
        for (let i = 0; i < cities.length; i++) {
            cities[i].addEventListener("click", () => {
                for (let j = 0; j < citiesBlock.length; j++) {
                    if (cities[i].dataset.city == citiesBlock[j].dataset.city) {
                        cities[i].classList.add("active");
                        citiesBlock[j].classList.add("map__contacts__block_active");
                    }
    
                    if (citiesBlock[j].classList.contains("map__contacts__block_active") && cities[i].dataset.city != citiesBlock[j].dataset.city) {
                        citiesBlock[j].classList.remove("map__contacts__block_active");
                    }
                }
    
                for (let j = 0; j < cities.length; j++) {
                    if (cities[i] != cities[j] && cities[j].classList.contains("active")) {
                        cities[j].classList.remove("active");
                    }
                }
            })
        }
    }

    // show sort for catalog

    let catalogBlock = document.querySelectorAll(".catalogProduct__container__content__form__container__block");

    if (catalogBlock.length > 0) {
        for (let i = 0; i < catalogBlock.length; i++) {
            catalogBlock[i].addEventListener("click", (e) => {
                if (!e.target.classList.contains("catalogProduct__container__content__form__container__block__drop")) {
                    if (catalogBlock[i].children[catalogBlock[i].children.length - 1].classList.contains("catalogProduct__container__content__form__container__block__drop_js")) {
                        catalogBlock[i].children[catalogBlock[i].children.length - 1].classList.remove("catalogProduct__container__content__form__container__block__drop_js")
                    } else {
                        catalogBlock[i].children[catalogBlock[i].children.length - 1].classList.add("catalogProduct__container__content__form__container__block__drop_js")
                    }
    
                    if (i == 2) {
                        for (let j = 0; j < catalogBlock[i].children[1].children.length; j++) {
                            if (catalogBlock[i].children[1].children[j].children[0].checked == true) {
                                catalogBlock[i].children[0].textContent = catalogBlock[i].children[1].children[j].children[0].value;
                            }
                        }
                    }
                }
            })
        }
    }

    // count checkboxes. Page catalog.

    let countCheckboxes = document.querySelectorAll(".catalogProduct__container__content__form__container__block__drop>div>div>input");
    let countBlock = document.querySelector(".catalogProduct__container__content__form__container__block__count");
    let count = 0;

    if (countCheckboxes.length > 0) {
        for (let i = 0; i < countCheckboxes.length; i++) {
            countCheckboxes[i].addEventListener("click", () => {
                if (countCheckboxes[i].checked == true) {
                    count++;
                    countBlock.children[0].textContent = count;
    
                    if (count != 0) {
                        countBlock.classList.add("catalogProduct__container__content__form__container__block__count_js");
                    } else {
                        countBlock.classList.remove("catalogProduct__container__content__form__container__block__count_js");
                    }
                } else {
                    count--;
                    countBlock.children[0].textContent = count;
    
                    if (count != 0) {
                        countBlock.classList.add("catalogProduct__container__content__form__container__block__count_js");
                    } else {
                        countBlock.classList.remove("catalogProduct__container__content__form__container__block__count_js");
                    }
                }
            })
        }
    }

    // change picture. Page of product.

    let smallPictures = document.querySelectorAll(".item__container__dopContainer__part1__slider__smallPictures__block>img");
    let bigPicture = document.querySelector(".item__container__dopContainer__part1__slider__bigPicture>img");

    if (smallPictures.length > 0) {
        for (let i = 0; i < smallPictures.length; i++) {
            smallPictures[i].addEventListener("click", () => {
                console.log(smallPictures[i].getAttribute("src"));
                bigPicture.setAttribute("src", smallPictures[i].getAttribute("src"));
                bigPicture.parentElement.setAttribute("href", smallPictures[i].getAttribute("src"));
            })
        }
    }

    // change page. Page of product.

    let textForChangePage = document.querySelectorAll(".item__container__dopContainer__part2__buttons>h6");
    let pages = document.querySelectorAll(".item__container__dopContainer__part2__container__content");

    if (textForChangePage.length > 0) {
        for (let i = 0; i < textForChangePage.length; i++) {
            textForChangePage[i].addEventListener("click", () => {
                textForChangePage[i].classList.add("item__container__dopContainer__part2__buttons_js");
    
                for (let j = 0; j < pages.length; j++) {
                    if (textForChangePage[i].dataset.page == pages[j].dataset.page) {
                        pages[j].classList.add("item__container__dopContainer__part2__container__content_js");
                    }
    
                    if (textForChangePage[i].dataset.page != pages[j].dataset.page && pages[j].classList.contains("item__container__dopContainer__part2__container__content_js")) {
                        pages[j].classList.remove("item__container__dopContainer__part2__container__content_js");
                    }
                }
    
                for (let j = 0; j < textForChangePage.length; j++) {
                    if (textForChangePage[i].dataset.page != textForChangePage[j].dataset.page && textForChangePage[j].classList.contains("item__container__dopContainer__part2__buttons_js")) {
                        textForChangePage[j].classList.remove("item__container__dopContainer__part2__buttons_js");
                    }
                }
            })
        }
    }

    // change input. Basket page.
    let amountBasket = document.querySelector(".basket__container__dopContainer__content__footer__part1__total>h6");
    let pricesBasket = document.querySelectorAll(".basket__container__dopContainer__content__body__block__total>p");
    
    let basketInputs = document.querySelectorAll(".basket__container__dopContainer__content__body__block__count__input");
    let orderPriceBasket = document.querySelector("#orderPriceBasket");
    let deliveryBasket = document.querySelector("#deliveryBasket");
    let orderSumBasket = document.querySelector(".basket__container__dopContainer__content__footer__part3__part1__sum");

    if (basketInputs.length > 0) {
        for (let i = 0; i < basketInputs.length; i++) {
            function changeTotalPrice() {
                let stringOne = basketInputs[i].parentElement.previousElementSibling.children[0].textContent;
                let arrayOne = stringOne.split(" ");
                let numberOne = +arrayOne[0];
                // console.log(numberOne);
                // console.log(numberOne * +basketInputs[i].value);
    
                let stringMany = basketInputs[i].parentElement.nextElementSibling.children[0].textContent = `${numberOne * +basketInputs[i].value} ₽`;
                let sumBasket = 0;
    
                for (let j = 0; j < pricesBasket.length; j++) {
                    let stringSum = pricesBasket[j].textContent;
                    let arraySum = stringSum.split(" ");
                    let numberSum = +arraySum[0];
                    sumBasket += numberSum;
    
                    let stringDelivery = deliveryBasket.textContent;
                    let arrayDelivery = stringDelivery.split(" ₽");
                    let numberDelivery = +arrayDelivery[0];
            
                    amountBasket.textContent = `${sumBasket} ₽`;
                    orderPriceBasket.textContent = `${sumBasket} ₽`;
                    orderSumBasket.textContent = `${sumBasket + numberDelivery} ₽`;
                }
            }
    
            changeTotalPrice();
    
            basketInputs[i].previousElementSibling.addEventListener("click", () => {
                if (basketInputs[i].value > 1) {
                    basketInputs[i].value = +basketInputs[i].value - 1;
                }
    
                changeTotalPrice();
            })
    
            basketInputs[i].addEventListener("input", () => {
                basketInputs[i].value = basketInputs[i].value.replace (/\D/g, '');
    
                changeTotalPrice();
            })
    
            basketInputs[i].nextElementSibling.addEventListener("click", () => {
                basketInputs[i].value = +basketInputs[i].value + 1;
    
                changeTotalPrice();
            })
        }
    
        // remove product. Basket page.
    
        let removeBtns = document.querySelectorAll(".basket__container__dopContainer__content__body__block__remove__btn");
    
        for (let i = 0; i < removeBtns.length; i++) {
            removeBtns[i].addEventListener("click", () => {
                removeBtns[i].parentElement.parentElement.remove();
    
                let pricesBasketAfterRemove = document.querySelectorAll(".basket__container__dopContainer__content__body__block__total>p");
                let sumBasket = 0;
    
                if (pricesBasketAfterRemove.length == 0) {
                    amountBasket.textContent = "0 ₽";
                }
    
                for (let j = 0; j < pricesBasketAfterRemove.length; j++) {
                    let stringSum = pricesBasketAfterRemove[j].textContent;
                    let arraySum = stringSum.split(" ");
                    let numberSum = +arraySum[0];
                    sumBasket += numberSum;
            
                    amountBasket.textContent = `${sumBasket} ₽`;
                }

                orderPriceBasket.textContent = `${sumBasket} ₽`;

                let stringSumForDeliveryBasket = deliveryBasket.textContent;
                let arraySumForDeliveryBasket = stringSumForDeliveryBasket.split(" ");
                let numberSumForDeliveryBasket = +arraySumForDeliveryBasket[0];

                let stringSumForOrderSumBasket = orderSumBasket.textContent;
                let arraySumForOrderSumBasket = stringSumForOrderSumBasket.split(" ");
                let numberSumForOrderSumBasket = +arraySumForOrderSumBasket[0];

                let readyNumberForSubtraction = numberSumForOrderSumBasket - sumBasket;
                let readyNumberForAdd = numberSumForOrderSumBasket + numberSumForDeliveryBasket;

                orderSumBasket.textContent = `${readyNumberForAdd - readyNumberForSubtraction} ₽`;
            })
        }
    
        // change delivery. Basket page.
    
        let radiosBasket = document.querySelectorAll(".basket__container__dopContainer__content__footer__part2__content");
        let radiosBasketContainer = radiosBasket[0];
    
        for (let i = 0; i < radiosBasketContainer.children.length; i++) {
            radiosBasketContainer.children[i].children[0].children[0].addEventListener("click", () => {
                let orderSumBasket2 = document.querySelector(".basket__container__dopContainer__content__footer__part3__part1__sum");
    
                if (radiosBasketContainer.children[i].children[0].children[0].checked && i == 1) {
                    console.log(1);
                    let stringDelivery = deliveryBasket.textContent;
                    let arrayDelivery = stringDelivery.split(" ₽");

                    if (+arrayDelivery[0] != 0) {
                        return;
                    }

                    let numberDelivery = +arrayDelivery[0] + 350;
    
                    let stringSum = orderSumBasket2.textContent;
                    let arraySum = stringSum.split(" ₽");
                    let numberSum = +arraySum[0];
    
                    console.log(numberSum);
    
                    deliveryBasket.textContent = `${numberDelivery} ₽`;
                    orderSumBasket.textContent = `${numberSum + numberDelivery} ₽`;
                } else {
                    console.log(2);
                    let stringSum = orderSumBasket2.textContent;
                    let arraySum = stringSum.split(" ₽");
                    let numberSum = +arraySum[0] - 350;
    
                    deliveryBasket.textContent = "0 ₽";
                    orderSumBasket.textContent = `${numberSum} ₽`;
                }
            })
        }
    }
});