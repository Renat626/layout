document.addEventListener("DOMContentLoaded", () => {
    // search for menu

    // let btnSearch = document.querySelector("#showFormSearch");
    // let formSearch = document.querySelector(".header__part2__container__search");

    // btnSearch.addEventListener("click", () => {
    //     formSearch.classList.add("header__part2__container__search_js");
    // })

    // scrollUp

    let scrollUp = document.querySelector(".scrollUp");

    window.addEventListener("scroll", () => {
        if (window.scrollY > 175) {
            scrollUp.classList.add("scrollUp_js");
        } else {
            scrollUp.classList.remove("scrollUp_js");
        }
    })

    // scrollHeader

    let scrollHeader = document.querySelector(".scrollHeader");

    window.addEventListener("scroll", () => {
        if (window.scrollY > 175) {
            scrollHeader.classList.add("scrollHeader_js");
        } else {
            scrollHeader.classList.remove("scrollHeader_js");
        }
    })

    // modal form

    let closeBtn = document.querySelector(".form__container__content>button");
    let openBtn = document.querySelectorAll(".callModalForm");
    let modalForm = document.querySelector(".form");

    for (let i = 0; i < openBtn.length; i++) {
        openBtn[i].addEventListener("click", () => {
            modalForm.classList.add("form_js");
        })
    }
    
    closeBtn.addEventListener("click", () => {
        modalForm.classList.remove("form_js");
    })

    document.addEventListener("click", (e) => {
        if (e.target.className == "form__background") {
            modalForm.classList.remove("form_js");
        }
    })

    // burger menu 

    let burgerBtn = document.querySelector(".header__part2__container__burger>i");
    let burgerMenu = document.querySelector(".header__part2__container>ul");

    burgerBtn.addEventListener("click", () => {
        burgerMenu.classList.toggle("header__part2__container_js");
    })

    // burger menu scroll

    let burgerBtnScroll = document.querySelector(".scrollHeader__container__links__burger>i");
    let burgerMenuScroll = document.querySelector(".scrollHeader__container__links>ul");

    burgerBtnScroll.addEventListener("click", () => {
        burgerMenuScroll.classList.toggle("scrollHeader__container__links_js");
        burgerBtnScroll.parentElement.classList.toggle("scrollHeader__container__links__burger_js");
    })

    // hide and show label for input of page contact

    let pageContactInputs = document.querySelectorAll(".contact__conteiner__part3__part2__container__form__container__smallInput>div>input");
    
    if (pageContactInputs != null) {
        for (let i = 0; i < pageContactInputs.length; i++) {
            pageContactInputs[i].addEventListener("focus", () => {
                pageContactInputs[i].nextElementSibling.classList.add("hideLabelJs");
            })
    
            pageContactInputs[i].addEventListener("focusout", () => {
                if (pageContactInputs[i].value == "") {
                    pageContactInputs[i].nextElementSibling.classList.remove("hideLabelJs");
                }
            })
        }
    }

    // hide and show label for textarea of page contact

    let pageContactTextarea = document.querySelector(".contact__conteiner__part3__part2__container__form__container__textarea>div>textarea");

    if (pageContactTextarea != null) {
        pageContactTextarea.addEventListener("focus", () => {
            pageContactTextarea.nextElementSibling.classList.add("hideLabelJs");
        })
    
        pageContactTextarea.addEventListener("focusout", () => {
            if (pageContactTextarea.value == "") {
                pageContactTextarea.nextElementSibling.classList.remove("hideLabelJs");
            }
        })
    }
})