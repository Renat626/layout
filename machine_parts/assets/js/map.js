document.addEventListener("DOMContentLoaded", () => {
    ymaps.ready(function () {
        var myMap = new ymaps.Map('map', {
                center: [55.192600, 61.370547],
                zoom: 16
            }, {
                searchControlProvider: 'yandex#search'
            }),
    
            // Создаём макет содержимого.
            MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
            ),
    
            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
                hintContent: 'Собственный значок метки',
                balloonContent: 'Это красивая метка'
            }, {
                // Опции.
                // Необходимо указать данный тип макета.
                iconLayout: 'default#image',
                // Своё изображение иконки метки.
                iconImageHref: '../assets/img/markForMap.png',
                // Размеры метки.
                iconImageSize: [82, 108],
                // Смещение левого верхнего угла иконки относительно
                // её "ножки" (точки привязки).
                iconImageOffset: [-50, -108]
            });
    
        myMap.geoObjects
            .add(myPlacemark)
    });
})