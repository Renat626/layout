document.addEventListener("DOMContentLoaded", () => {
    const firstSlider = new Swiper('.firstSlider__container', {
        slidesPerView: 1,
        // pagination: {
        //     el: '.firstSlider__container__leaf__pagination',
        //     type: 'fraction',
        // },
        // navigation: {
        //     nextEl: '.firstSlider__container__leaf__btn_right',
        //     prevEl: '.firstSlider__container__leaf__btn_left',
        // },
    });

    const productInfoSliderThumbs = new Swiper('.productInfo__container__slider__thumbs', {
        breakpoints: {
            1401: {
                slidesPerView: 5
            },
            1131: {
                slidesPerView: 4
            },
            821: {
                slidesPerView: 3
            },
            701: {
                slidesPerView: 5
            },
            561: {
                slidesPerView: 4
            },
            408: {
                slidesPerView: 3
            },
            0: {
                slidesPerView: 2
            }
        },
        spaceBetween: 30,
        pagination: {
            el: '.productInfo__container__slider__thumbs__navigation__pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.productInfo__container__slider__thumbs__navigation__btnRight',
            prevEl: '.productInfo__container__slider__thumbs__navigation__btnLeft',
        }
    });

    const productInfoSlider = new Swiper('.productInfo__container__slider__top', {
        slidesPerView: 1,
        thumbs: {
            swiper: productInfoSliderThumbs
        }
    });

    const seeAlsoSlider = new Swiper('.seeAlso__container__slider', {
        breakpoints: {
            1341: {
                spaceBetween: 170,
                slidesPerView: 2
            },
            1271: {
                spaceBetween: 100,
                slidesPerView: 2
            },
            1001: {
                spaceBetween: 50,
                slidesPerView: 2
            },
            731: {
                spaceBetween: 20,
                slidesPerView: 2
            },
            0: {
                spaceBetween: 0,
                slidesPerView: 1
            }
        },
        slidesPerView: 2,
        spaceBetween: 170,
        pagination: {
            el: '.seeAlso__container__slider__header__navigation__pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.seeAlso__container__slider__header__navigation__btnRight',
            prevEl: '.seeAlso__container__slider__header__navigation__btnLeft',
        },
    });
});